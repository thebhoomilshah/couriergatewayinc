@extends('layout')

@section('content')
<h1>Country List</h1>
<hr>
<table class="table table-striped mt-5">
    <thead>
      <tr>
        <th scope="col">Country Code</th>
        <th scope="col">Name</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($countries as $country)
            <tr>
                <th scope="row">{{$country['countryCode']}}</th>
                <td>{{$country['name']}}</td>
                <td><a class="btn btn-dark" href="{{url('country').'/'.$country['countryCode']}}"> <i class="fa fa-eye"></i></a></td>
            </tr>
        @endforeach

    </tbody>
  </table>
  {{$countries->links('pagination::bootstrap-4')}}

@endsection
