@extends('layout')

@section('content')
<h1>Country Information</h1>
<hr>
<table class="table table-bordered mt-5">
    <tbody>
      <tr>
        <th scope="row">Common Name</th>
        <td>{{$countryInfo['commonName']}}</td>
      </tr>
      <tr>
        <th scope="row">Official Name</th>
        <td>{{$countryInfo['officialName']}}</td>
      </tr>
      <tr>
        <th scope="row">Country Code</th>
        <td>{{$countryInfo['countryCode']}}</td>
      </tr>
      <tr>
        <th scope="row">Region</th>
        <td>{{$countryInfo['region']}}</td>
      </tr>

      <tr>
        <th scope="row">No. of Countries Bordering</th>
        <td>{{count($countryInfo['borders'])}}</td>
      </tr>

    </tbody>
  </table>
<h1 class="mt-5">Public Holidays</h1>
<hr>
<p>Total Public Holidays this Year: {{count($publicHolidays)}}</p>
<h3 class="mt-5">Next 4 holidays</h3>

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Date</th>
        <th scope="col">Local Name</th>
        <th scope="col">Name</th>
        <th scope="col">Country Code</th>
      </tr>
    </thead>
    <tbody>
        @php
            $count = 0;
        @endphp
        @foreach ($publicHolidays as $holiday)
            @if(strtotime($holiday['date']) > strtotime(now()) && $count < 4)

            <tr>
                <th scope="row">{{$holiday['date']}}</th>
                <td>{{$holiday['localName']}}</td>
                <td>{{$holiday['name']}}</td>
                <td>{{$holiday['countryCode']}}</td>
            </tr>
            @php
                $count++;
            @endphp
            @endif

        @endforeach

    </tbody>
  </table>

@endsection
